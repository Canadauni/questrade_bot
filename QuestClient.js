const axios = require('axios').default
const fs = require('fs')
const closestIndexTo = require('date-fns/closestIndexTo')
const chrono = require('chrono-node')

class QuestClient {
  constructor(refreshToken, accessToken, apiServer) {
    this.refreshToken = process.env.QUEST_TOKEN
    this.accessToken = ''
    this.apiServer = ''
  }

  async getAccessToken() {
    const res = await axios.get(`https://login.questrade.com/oauth2/token?grant_type=refresh_token&refresh_token=${this.refreshToken}`)
    const data = res.data
    await this.updateToken(this.refreshToken, data.refresh_token)
    this.refreshToken = data.refresh_token
    this.accessToken = data.access_token
    this.apiServer = data.api_server
    return
  }

  async updateToken(oldToken, newToken) {
    let env = await fs.promises.readFile('.env','utf-8')
    env = env.replace(oldToken, newToken)
    await fs.promises.writeFile('.env', env, 'utf-8')
  }

  async getSymbolId(symbol) {
    const res = await axios.get(`${this.apiServer}v1/symbols/search?prefix=${symbol}`, {headers:{'Authorization': `Bearer ${this.accessToken}`}})
    return res.data.symbols[0]
  }

  async getQuote(id) {
    const res = await axios.get(`${this.apiServer}v1/markets/quotes?ids=${id}`, {headers:{'Authorization': `Bearer ${this.accessToken}`}})
    return res.data.quotes[0]
  }

  async getOptions(date, id) {
    const res = await axios.get(`${this.apiServer}v1/symbols/${id}/options`, {headers:{'Authorization': `Bearer ${this.accessToken}`}})
    const expiryDates = res.data.optionChain.map(i => new Date(i.expiryDate))
    const expIndex = closestIndexTo(chrono.parseDate(date), expiryDates)
    return res.data.optionChain[expIndex]
  }

  async getContractId(strike,type,optionChain) {
    const strikeChain = optionChain.chainPerRoot[0].chainPerStrikePrice
    const result = strikeChain.find(i => i.strikePrice === strike)
    return result[`${type}SymbolId`]
  }
}

module.exports = {
  QuestClient
}