require('dotenv').config()
const QuestClient = require('./QuestClient').QuestClient
const Discord = require('discord.js')

const discordClient = new Discord.Client()
const questClient = new QuestClient()

discordClient.once('ready', () => {
  console.log('ready!')
})

discordClient.on('message', async message => {
  const regexp = /([$])\D\S+/g
  if(!message.author.bot && message.content.includes('$')) {
    await questClient.getAccessToken()
    const symbols = Array.from(message.content.matchAll(regexp), m => m[0].toUpperCase())
    const quotes = []
    symbols.forEach(async i => {
      try {
        const results = await questClient.getSymbolId(i.slice(1))
        const quote = await questClient.getQuote(results.symbolId)
        const pctChange = (quote.lastTradePrice - quote.openPrice) / quote.openPrice
        quotes.push(`Last price of ${i} was $${quote.lastTradePrice} (${(pctChange*100).toFixed(2)}%)`)
      }
      catch(err) {
        quotes.push(`Couldn't find the symbol you asked for`)
      }
    })
    message.channel.send(quotes.join('\n'))
  }
})

discordClient.login(process.env.DISCORD_TOKEN)

async function detectOption(content) {
  const regexp = /\d+(?=(c|p))\w+?/g
  const optionsList = Array.from(message.content.matchAll(regexp), m => m[0])
  console.log(optionsList)
}